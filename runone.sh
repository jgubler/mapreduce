#!/bin/bash

user="luke"
port="3410"
ipandport=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:$port
ipaddress=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`
server="0"
client="1"
location="/home/j/jgubler/cs3410/src/map"
sqliteinput="/home/j/jgubler/cs3410/src/map/pride.sqlite3"
sqliteoutput="/home/j/jgubler/cs3410/src/map/shame.sqlite3"

### how to use ###
# go
# run
# main.go
# ipaddress of server including port
# true/false true is the master, workers are false

echo "---------THIS SERVER---------"
go run $location/main.go $ipandport $server $sqliteinput $sqliteoutput

