#!/bin/bash

user="jgubler"
port="3410"
ipaddress00=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:$port
ipaddress01=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3412
ipaddress02=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3413
ipaddress03=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3414
ipaddress04=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3415
ipaddress05=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3416
ipaddress06=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3417
ipaddress07=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3418
ipaddress08=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:3419
ipaddress09=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:9420
server="0"
client="1"
location="/home/j/jgubler/cs3410/src/map"
sqliteinput="/home/j/jgubler/cs3410/src/map/pride.sqlite3"
sqliteoutput="/home/j/jgubler/cs3410/src/map/shame.sqlite3"

#echo $ipaddress

### how to use ###
# go
# run
# main.go
# ipaddress of server including port
# true/false true is the master, workers are false
#                          go run $location/main.go $ipandport $server $sqliteinput $sqliteoutput
(echo "---------NITROGEN---------" &) &&
(ps -ef | go run ${location}/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------OXYGEN---------" &) &&
(ps -ef | go run $location/main.go  $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------CALCIUM---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------POTASSIUM---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------CHLORINE---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------ALUMINUM---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------SILICON---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------MAGNESIUM---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------PHOSPHORUS---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &) &&
(echo "---------SODIUM---------" &) &&
(ps -ef | go run $location/main.go $ipaddress00 $client $sqliteinput $sqliteoutput &)

#echo "---------NITROGEN---------"
#ssh $user@nitrogen "ifconfig eth0 | grep 'inet addr'"
#echo "---------OXYGEN---------"
#ssh $user@oxygen "ifconfig eth0 | grep 'inet addr'"
#echo "---------CALCIUM---------"
#ssh $user@calcium "ifconfig eth0 | grep 'inet addr'"
#echo "---------POTASSIUM---------"
#ssh $user@potassium "ifconfig eth0 | grep 'inet addr'"
#echo "---------CHLORINE---------"
#ssh $user@chlorine "ifconfig eth0 | grep 'inet addr'"
#echo "---------ALUMINUM---------"
#ssh $user@aluminum "ifconfig eth0 | grep 'inet addr'"
#echo "---------SILICON---------"
#ssh $user@silicon "ifconfig eth0 | grep 'inet addr'"
#echo "---------MAGNESIUM---------"
#ssh $user@magnesium "ifconfig eth0 | grep 'inet addr'"
#echo "---------PHOSPHORUS---------"
#ssh $user@phosphorus "ifconfig eth0 | grep 'inet addr'"
#echo "---------SODIUM---------"
#ssh $user@sodium "ifconfig eth0 | grep 'inet addr'"

