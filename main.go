package main

import (
	"./mapreduce"
	"log"
	"strconv"
	"strings"
	"unicode"
)

func wordCountMapper(key, value string, output chan<- mapreduce.Pair) error {
	//defer close(output)
	lst := strings.Fields(value)
	for _, elt := range lst {
		word := strings.Map(func(r rune) rune {
			if unicode.IsLetter(r) || unicode.IsDigit(r) {
				return unicode.ToLower(r)
			}
			return -1
		}, elt)
		if len(word) > 0 {
			output <- mapreduce.Pair{Key: word, Value: "1"}
		}
	}
	return nil
}

func wordCountReducer(key string, values <-chan string, output chan<- mapreduce.Pair) error {
	//defer close(output)
	count := 0
	for v, ok := <-values; ok; v, ok = <-values {
		i, err := strconv.Atoi(v)
		if err != nil {
			return err
		}
		//log.Printf("V: %v, K: %v", v, ok)
		count += i
	}
	p := mapreduce.Pair{Key: key, Value: strconv.Itoa(count)}
	output <- p
	return nil
}

func main() {
	if err := mapreduce.Start(mapreduce.Mapper(wordCountMapper), mapreduce.Reducer(wordCountReducer)); err != nil {
		log.Fatal(err)
	}
	
}
