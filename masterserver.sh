#!/bin/bash

user="lhansen"
port="3410"
ipaddress=`ifconfig eth0 | grep 'inet addr' | awk -F: '{print $2}' | awk '{print $1}'`:$port
server="0"
client="1"

#echo $ipaddress

### how to use ###
# go
# run
# main.go
# ipaddress of server including port
# true/false true is the master, workers are false

echo "----------THIS MACHINE----------"
go run ~/cs3410/mapreduce/main.go $ipaddress $server
