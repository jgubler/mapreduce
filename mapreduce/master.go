package mapreduce

import (
	"database/sql"
//	"flag"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
    
    "strings"
    "time"
)

type Mapper func(key, value string, output chan<- Pair) error
type Reducer func(key string, values <-chan string, output chan<- Pair) error

func Start(mapper Mapper, reducer Reducer) error {
	master, err := strconv.Atoi(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	source := "pride.sqlite3"
	target := "shame.sqlite3"
	tmp := "/tmp"
	// will figure this out maybe later
	m := 10
	r := 5
	port := 3410
	//address := getLocalAddress() + ":" + strconv.Itoa(port)
	serveraddress := os.Args[1]
	log.Printf("serveraddress: %v, programmode: %v", serveraddress, master)
	myaddress := net.JoinHostPort(getLocalAddress(), fmt.Sprintf("%d", port))

	if master == 0 {
		targetdir := filepath.Dir(target)
		return startMaster(myaddress, source, targetdir, target, m, r)
	} else if master == 1 {
		return startWorker(myaddress, serveraddress, mapper, reducer, tmp)
	} else {
	  log.Printf("You didn't identify a server/worker status")
	  return nil
	}
}

//startmaster carves up the input by using makemaptasklist
func startMaster(myaddress string, source string, targetdir string, target string, m int, r int) error {
	cmd1 := exec.Command("rm", target) // erase the stuff directory if it is there
		
    err := cmd1.Run()
    if err != nil {
		log.Printf("%v", err)
	}

    err = os.Mkdir("stuff/", 0755) // makes a directory
	if err != nil {
		log.Fatal(err)
	}

	master := &Master{
		Address:   myaddress,
		Source:    source,
		TargetDir: targetdir,
		Target:    target,
		M:         m,
		R:         r,
        MapTasks:  0,
        FinishedReduceTasks: 0,
		//don't know if this is the right size of this slice
		//WorkTasks: make([]WorkResponse, 0, m+m*r),
	}
	//break source up into chunks
	sourceDB, err := sql.Open("sqlite3", source)
	rows, _ := sourceDB.Query("SELECT count(*) from pairs;")
	var pairNum int
	for rows.Next() {
		err = rows.Scan(&pairNum)
		if err == nil {
			fmt.Println("Count:", pairNum)
		} else {
			log.Fatalf("%v", err)
		}
	}
	if err != nil {
		log.Fatal(err)
	}
	defer sourceDB.Close()
	var chunkNum int
	for chunkNum = 1; chunkNum <= m; chunkNum++ { // remainders will be lost because of division without modulo
		mapTask := MapTask{
			M:        m,
			R:        r,
			N:        chunkNum,
			Offset:   ((chunkNum - 1) * (pairNum / m)) + 1,
			Limit:    ((chunkNum) * (pairNum / m)),
			Address:  myaddress,
			Source:   source,
			Complete: false,
		}
        log.Printf("offset %v, limit %v", mapTask.Offset, mapTask.Limit)
		workTask := WorkResponse{
			Type: "map",
			Map:  &mapTask,
		}
		master.WorkTasks = append(master.WorkTasks, workTask)
	}
	somenum := pairNum % m // addresses the modulo portion if there is a remainder
	if somenum != 0 {
		chunkNum = m + 1
		mapTask := MapTask{
			M:        m,
			R:        r,
			N:        chunkNum,
			Offset:   ((chunkNum - 1) * (pairNum / m)),
			Limit:    ((chunkNum - 1) * (pairNum / m)) + somenum,
			Address:  myaddress,
			Source:   source,
			Complete: false,
		}
        log.Printf("offset %v, limit %v", mapTask.Offset, mapTask.Limit)
		workTask := WorkResponse{
			Type: "map",
			Map:  &mapTask,
		}
		master.WorkTasks = append(master.WorkTasks, workTask)
	}
    for redTask := 1; redTask <= master.R; redTask++{
        reduceTask := ReduceTask {
            M: master.M,
            R: master.R,
            N: redTask,
            Address: myaddress,
            MapHosts: make([]string,master.M+2,master.M+2),
            Target: master.Target,
            Complete: false,
        }
        workTask := WorkResponse{
			Type: "reduce",
			Reduce:  &reduceTask,
		}
        master.ReduceTasks = append(master.ReduceTasks,workTask)
    }

	masterServer := make(Server, 1)
	masterServer <- master
	rpc.Register(masterServer)
    shutdownChannel := make(ShutdownChannel, 1)
//	rpc.Register(shutdownChannel)
    master.Shutdown = shutdownChannel
	rpc.HandleHTTP()

	listen, err := net.Listen("tcp", myaddress)
	if err != nil {
		log.Fatal("listen error:", err)
	}
	
	go http.Serve(listen, nil)
	<- shutdownChannel
    log.Printf("Merging Reducer Output")
    time.Sleep((10 * time.Second))
    // do the master merge here!
    outputDb, err := sql.Open("sqlite3", master.Target)
    if err != nil {
        log.Printf("%v", err)
    }
    
    sql2 := "create table pairs (key string, value string);"
    _, err = outputDb.Exec(sql2)
    if err != nil {
        log.Printf("%q: %s\n", err, sql2)
    }
    
    defer outputDb.Close()
    //gather mapper info into input db
    for index := 1; index <= master.R; index++{
        if index != 0{
            gatherInto("stuff/reduce"+strconv.Itoa(index)+"output.sql",outputDb)
        }
    }

    cmd := exec.Command("rm", "-r", "stuff/") // erase the stuff directory cleanup
		
    err1 := cmd.Run()
    if err != nil {
		log.Printf("%v", err1)
	}

	return nil
}


func (server Server) GetWork(request WorkRequest, reply *WorkResponse) error {
	master := <-server
		if request.MapFinished != -1 {
			// if the worker finished a map task it will return which number it was
//            
            log.Printf("MapTask #%v finished",request.MapFinished)
            
            for _, redTask := range master.ReduceTasks {
                redTask.Reduce.MapHosts[request.MapFinished] = request.Address
            }
            master.MapTasks += 1
            if master.MapTasks < master.M+1 {
                if len(master.WorkTasks) > 0 {
                    log.Printf("map task #%v started",master.WorkTasks[0].Map.N)
                    *reply = master.WorkTasks[0]
			        master.WorkTasks = master.WorkTasks[1:]
                } else {
		            //tell them there are no cookies left so they can quit.
		            reply.Type = "wait"
	            }
            } else {
                if len(master.ReduceTasks) > 0 {
                    log.Printf("reduce task #%v started",master.ReduceTasks[0].Reduce.N)
                    *reply = master.ReduceTasks[0]
			        master.ReduceTasks = master.ReduceTasks[1:]
                } else {
		            //tell them there are no cookies left so they can quit.
		            reply.Type = "done"
	            }
            }
		} else if request.ReduceFinished != -1 {
			// if the reducer finished a reduce task it will return which reduce it was, maybe modded we'll see
            log.Printf("ReduceTask #%v finished", request.ReduceFinished)
            master.FinishedReduceTasks += 1
            if master.FinishedReduceTasks >= master.R {
                master.Shutdown <- true
            }
            if len(master.ReduceTasks) > 0 {
                log.Printf("reduce task #%v started",master.ReduceTasks[0].Reduce.N)
                *reply = master.ReduceTasks[0]
			    master.ReduceTasks = master.ReduceTasks[1:]
            } else {
		        //tell them there are no cookies left so they can quit.
		        reply.Type = "done"
	        }
            
		} else {
			// this worker hasn't done anything yet, give him a number, and get him some stuff
            if master.MapTasks < master.M+1 {
                if len(master.WorkTasks) > 0 {
                    log.Printf("map task #%v started",master.WorkTasks[0].Map.N)
                    *reply = master.WorkTasks[0]
			        master.WorkTasks = master.WorkTasks[1:]
                } else {
		            //tell them there are no cookies left so they can quit.
		            reply.Type = "wait"
	            }
            } else {
                if len(master.ReduceTasks) > 0 {
                    log.Printf("reduce task #%v started",master.ReduceTasks[0].Reduce.N)
                    *reply = master.ReduceTasks[0]
			        master.ReduceTasks = master.ReduceTasks[1:]
                } else {
		            //tell them there are no cookies left so they can quit.
		            reply.Type = "done"
	            }
            }
		}
	
	server <- master
	return nil
}

/////////////////////////////////////////////////////////////////////////////
func startWorker(myaddress string, address string, mapper Mapper, reducer Reducer, tmp string) error {
	var done bool
	done = false
	mapFinished := -1
	reduceFinished := -1
	for !done {
		request := WorkRequest{
			Address:        address,
			MapFinished:    mapFinished,
			ReduceFinished: reduceFinished,
		}
        //log.Printf("before call getWork")
		var reply WorkResponse
		Call(address, "Server.GetWork", request, &reply)
		
		if reply.Type == "done" {
			//master says we're done, you can now finish worker man!
            log.Printf("Jobs Done, Shutting Down")
			done = true
		}   else if reply.Type == "wait"{
            mapFinished = -1
	        reduceFinished = -1
            log.Printf("Waiting for more work")
            time.Sleep((5 * time.Second))
        }   else if reply.Type == "map" {
			//do a map function or die!
            log.Printf("starting up maptask %v", reply.Map.N)
			
            err := os.Mkdir("stuff/worker"+strconv.Itoa(reply.Map.N), 0755) // makes a directory
	        if err != nil {
		        log.Fatal(err)
	        }
	   // log.Printf("made dir")
            //log.Printf("creating mapper db %v", reply.Map.N)
            var dbHolder [] *sql.DB
            for i := 1; i <= reply.Map.R; i++ {
			    localDb, err := sql.Open("sqlite3", "stuff/worker"+strconv.Itoa(reply.Map.N)+"/map_out_"+strconv.Itoa(i)+".sql")
			    if err != nil {
				    log.Printf("%v", err)
			    }
                
                sql := "create table pairs (key string, value string);"
			    _, err = localDb.Exec(sql)
			    if err != nil {
				    log.Printf("%q: %s\n", err, sql)
                
			    }
                dbHolder = append(dbHolder,localDb)
			    
            }
           // log.Printf("size of DBHOLDER: %v", len(dbHolder))
			sourceDB, err := sql.Open("sqlite3", reply.Map.Source)
			if err != nil {
				log.Fatal(err)
			}
			defer sourceDB.Close()
            //log.Printf("sourceDB make pairs table")
			
            //log.Printf("sourceDB executed?")
			/////////do the database map thing//////////////
			//takes the pairs off the outputs and inserts into
            var output chan Pair
			output = make(chan Pair, 1)
           // log.Printf("before iter go loop")
			go func() {
				for o := range output {
					tup := fmt.Sprintf("values('%v', '%v')", o.Key, o.Value)
                    firstLetter := o.Key[:1]
                    //log.Printf("firstLetter = :%v", firstLetter)
                    letterNum := strings.Index(alphabet,firstLetter)
                    if letterNum == -1 {
                        letterNum = 0
                    }
                    //log.Printf("the letter num is: %v, send to database: %v",letterNum,letterNum % reply.Map.R)
					_, err = dbHolder[letterNum % reply.Map.R].Exec("insert into pairs (key, value) " + tup)
					
					if err != nil {
						log.Printf("Error writing to database : %v", err)
					}
                    
				}
                for _, db := range dbHolder {
                    db.Close()
                }
                
			}()
			//map the range that has been given
			start := reply.Map.Offset
			end := reply.Map.Limit
			stmt, err1 := sourceDB.Prepare("select value from pairs where rowid = ?")
            //log.Printf("sourceDB before error executed?")
			if err1 != nil {
				log.Fatal(err1)
			}
            //log.Printf("statement Prepared?")
			defer stmt.Close()
           // log.Printf("before iter loop")
			for iter := start; iter <= end; iter++ {
                		//log.Printf("iter: %v, end: %v",iter,end)
				var value string
				index := strconv.Itoa(iter)
                //log.Printf("execute Statement, index: %v",index)
				err = stmt.QueryRow(index).Scan(&value)
                //log.Printf("value is %v", value)
                
			    if err != nil {
				    log.Fatal(err)
			    }
                //log.Printf("do the mapper ")
			    err = mapper("", value, output)
			    if err != nil {
				    log.Printf("Error mapping : %v", err)
			    }
                //log.Printf("after mapper")
			}
            //log.Printf("end of mapper")
            time.Sleep((5 * time.Second))
			//reset variables////////////////////////
			mapFinished = reply.Map.N
			reduceFinished = -1
		} else if reply.Type == "reduce" {
		log.Printf("starting up reducer task %v", reply.Reduce.N)
            //make input db
            sourceDb, err := sql.Open("sqlite3", "stuff/reduce"+strconv.Itoa(reply.Reduce.N)+"input.sql")
	        if err != nil {
                log.Printf("opening up source db error")
		        log.Printf("%v", err)
                
	        }
            
            sql2 := "create table pairs (key string, value string);"
	        _, err = sourceDb.Exec(sql2)
	        if err != nil {
                log.Printf("sourceDB query error")
		        log.Printf("%q: %s\n", err, sql2)
                
	        }
            
	        defer sourceDb.Close()
            //gather mapper info into input db
            for index,_ := range reply.Reduce.MapHosts{
		        if index != 0{
	                        gatherInto("stuff/worker"+strconv.Itoa(index)+"/map_out_"+strconv.Itoa(reply.Reduce.N)+".sql",sourceDb)
		        }
            }
            //create output db
            localDb, err := sql.Open("sqlite3", "stuff/reduce"+strconv.Itoa(reply.Reduce.N)+"output.sql")
	        if err != nil {
                log.Printf("opening up output db error")
		        log.Printf("%v", err)
                
	        }
            
            sql := "create table pairs (key string, value string);"
	        _, err = localDb.Exec(sql)
	        if err != nil {
                log.Printf("query local db error")
		        log.Printf("%q: %s\n", err, sql)
                
	        }
            
	      

            var output chan Pair
			output = make(chan Pair, 1)
			go func() {
				for o := range output {
					tup := fmt.Sprintf("values('%v', '%v')", o.Key, o.Value)
                    //log.Printf("the letter num is: %v, send to database: %v",letterNum,letterNum % reply.Map.R)
					_, err = localDb.Exec("insert into pairs (key, value) " + tup)
					
					if err != nil {
						log.Printf("Error writing to database in output sink: %v", err)
					}
				}
                localDb.Close()
			}()
			//map the range that has been given
            rows, err := sourceDb.Query("SELECT DISTINCT key, value FROM pairs ORDER BY key")
            if err != nil {
                    log.Printf("rows query error")
                    log.Fatal(err)
                    
            }
            
            for rows.Next() {
                var input chan string = make(chan string, 1)
                var key string
                var value string
                rows.Scan(&key, &value)
//                log.Printf("Key: %v",key)
                selectedRows, err := sourceDb.Query("SELECT key, value FROM pairs WHERE key = '" + key + "'")
                if err != nil {
                        log.Printf("selected rows query error")
                        log.Fatal(err)
                        
                }
                 go func(inputChan chan string,newKey string){
                    //log.Printf("i am here after reducer starts in the go function") 
                    err = reducer(key, inputChan, output)
                    if err != nil {
			                log.Printf("Error reducing : %v", err)
		            }               
		            
			    }(input,key)
                for selectedRows.Next() {
                    var selectKey string
                    var selectValue string
                    rows.Scan(&selectKey, &selectValue)
//                    log.Printf("key: %v,value: %v", selectKey,selectValue)
                    input <- selectValue
                }
                selectedRows.Close()
               
                
//                log.Printf("made to after the reducer")
                close(input)
            }
            rows.Close()
			
			mapFinished = -1
			reduceFinished = reply.Reduce.N
		} else {
			log.Printf("the worker did not get a response")
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////
func getLocalAddress() string {
	var localaddress string
	ifaces, err := net.Interfaces()
	if err != nil {
		panic("init: failed to find network interfaces")
	}
	// find the first non-loopback interface with an IP address
	for _, elt := range ifaces {
		if elt.Flags&net.FlagLoopback == 0 && elt.Flags&net.FlagUp != 0 {
			addrs, err := elt.Addrs()
			if err != nil {
				panic("init: failed to get addresses for network interface")
			}
			for _, addr := range addrs {
				if ipnet, ok := addr.(*net.IPNet); ok {
					if ip4 := ipnet.IP.To4(); len(ip4) == net.IPv4len {
						localaddress = ip4.String()
						break
					}
				}
			}
		}
	}
	if localaddress == "" {
		panic("init: failed to find non-loopback interface with valid address on this node")
	}
	return localaddress
} //we've done this before

func gatherInto(path string, db *sql.DB) error {
	if _, err := db.Exec("attach ? as merge", path); err != nil {
		log.Printf("Error in attach command: %v", err)
	}
	if _, err := db.Exec("begin"); err != nil {
		log.Printf("Error in begin command: %v", err)
	}
	if _, err := db.Exec("insert into pairs select * from merge.pairs"); err != nil {
		log.Printf("Error in insert command: %v", err)
	}
	if _, err := db.Exec("commit"); err != nil {
		log.Printf("Error in commit command: %v", err)
	}
	if _, err := db.Exec("detach merge"); err != nil {
		log.Printf("Error in detach command: %v", err)
	}
	return os.Remove(path)
}
