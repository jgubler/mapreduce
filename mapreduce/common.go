package mapreduce

import (
	"log"
	"net/rpc"
)

type Pair struct {
	Key, Value string
}

type MapTask struct {
	M, R          int
	N             int
	Offset, Limit int
	Address       string
	Source        string
	Complete      bool
}

type ReduceTask struct {
	M, R     int
	N        int
	Address  string
	MapHosts []string
	Target   string
	Complete bool
}

type WorkRequest struct {
	Address        string
	MapFinished    int
	ReduceFinished int
}

type WorkResponse struct {
	Type   string
	Map    *MapTask
	Reduce *ReduceTask
}

//startMaster(myaddress string, source string, targetdir string, target string, m int, r int)
type Master struct {
	Address   string
	Source    string
	TargetDir string
	Target    string
	M         int
	R         int
	WorkTasks []WorkResponse
    MapTasks int
    ReduceTasks []WorkResponse
    FinishedReduceTasks int
    Shutdown chan bool
}

type ShutdownChannel chan bool

var alphabet string = "abcdefghijklmnopqrstuvwxyz "
type Server chan *Master

func Call(address string, method string, request WorkRequest, reply *WorkResponse) error {
	client, err := rpc.DialHTTP("tcp", address)
	defer client.Close()
	if err != nil {
		log.Fatalf("error connecting to server at %s: %v", address, err)
	}
	if err = client.Call(method, request, reply); err != nil {
		log.Fatalf("Error calling %v : %v", method, err)
	}
	return nil
}
